//
//  main.m
//  Blood Pressure
//
//  Created by PARNEET CHUGH on 01/04/14.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Blood_PressureAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([Blood_PressureAppDelegate class]));
    }
}
