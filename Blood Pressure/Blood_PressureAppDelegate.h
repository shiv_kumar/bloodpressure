//
//  Blood_PressureAppDelegate.h
//  Blood Pressure
//
//  Created by PARNEET CHUGH on 01/04/14.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//
//
//
// Master Changes

#import <UIKit/UIKit.h>

@interface Blood_PressureAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
